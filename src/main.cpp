#include "Arduino.h"
#include <SoftwareSerial.h>
#include <elapsedMillis.h>
#include <Button.h>

const bool DEBUG = true;
const bool SEND_POTENTIOMETER = false;

const int USB_BAUD_RATES  = 9600;
const int HM10_BAUD_RATES = 9600;

const unsigned int INTERVAL_TEST   = 3000;
const unsigned int INTERVAL_ANALOG = 300;

const int POTENTIOMETER_PIN = 0;
const int POTENTIOMETER_INTERVAL = 30;

const unsigned int MAX_NOTIF_LED_DURATION = 8000;

struct RGB_PINS {
  int r;
  int g;
  int b;
};

// leds
const RGB_PINS moodPins = { 3, 5, 6 };
const RGB_PINS notifPins = { 9, 10, 11 };

//rgb.255.1.1     red
//rgb.20.255.1    green
//rgb.20.1.255    blue
//rgb.100.250.255 white
//rgb.220.200.1   yellow
struct RGB {
  byte r;
  byte g;
  byte b;
};

RGB moodLed = { 0, 0, 0};
float pulseMoodLed = 0; // 0-1

RGB notifLed = {0, 0, 0};
elapsedMillis notifLedDuration;

const int BUTTON_PIN = 2;
Button doNotDisturbButton(BUTTON_PIN);

enum CommandState {
  READING_COMMAND,
  READING_R,
  READING_G,
  READING_B,
  NEW_DATA
};

CommandState commandState = READING_COMMAND;

struct Command {
  String command;
  String currentArg;
  String r;
  String g;
  String b;
};

Command command;

SoftwareSerial BTserial(7, 8);

int potentiometerValue = 0;

elapsedMillis sendTestElapsed;
elapsedMillis readAnalogElapsed;

const unsigned long PULSE_LEN = 5000UL;

void setup() {
  // USB
  Serial.begin(USB_BAUD_RATES);
  // while (!Serial);

  Serial.print("Sketch:   ");
  Serial.println(__FILE__);
  Serial.print("Uploaded: ");
  Serial.println(__DATE__);
  Serial.println(" ");

  BTserial.begin(HM10_BAUD_RATES);
  Serial.println("BTserial started at 9600");

  doNotDisturbButton.begin();

  pinMode(moodPins.r, OUTPUT);
  pinMode(moodPins.g, OUTPUT);
  pinMode(moodPins.b, OUTPUT);

  analogWrite(moodPins.r, 0);
  analogWrite(moodPins.g, 0);
  analogWrite(moodPins.b, 0);

}

void sendPotentiometerValue(int val) {
  BTserial.print("pot.");
  BTserial.println(val);

  if (DEBUG) {
    Serial.print("[potentiometerValue] ");
    Serial.println(val);
  }
}

void readPotentiometer() {
  if (readAnalogElapsed > INTERVAL_ANALOG) {
    int v1 = analogRead(POTENTIOMETER_PIN);
    int v2 = constrain(v1, 250 , 720);
    int v3 = map(v2, 250, 720, 0, 255);

    if (abs(potentiometerValue - v3) > POTENTIOMETER_INTERVAL) {
      potentiometerValue = v3;

      if (SEND_POTENTIOMETER) {
        sendPotentiometerValue(potentiometerValue);
      }
    }

    readAnalogElapsed = 0;
  }
}

void readSerial() {
  static boolean NL = true;
  while (Serial.available() > 0) {
    char c = Serial.read();

    // do not send line end characters to the HM-10
    if (c != 10 && c != 13 ) {
      BTserial.write(c);
    }

    // Echo the user input to the main window.
    // If there is a new line print the ">" character.
    if (NL) {
      Serial.print("\r\n>");
      NL = false;
    }

    Serial.write(c);

    if (c == 10) {
      NL = true;
    }
  }
}

bool readingCommandArg(CommandState state) {
  if (state == READING_R || state == READING_G || state == READING_B) {
    return true;
  }

  return false;
};

void readBluetooth() {
 while (BTserial.available() > 0) {
    char rc = BTserial.read();
    if (DEBUG) {
      Serial.print(rc);
    }

    if (rc == '\n') {
      if (DEBUG) {
        Serial.println("[end char]");
      }
      commandState = NEW_DATA;
      command.b = String(command.currentArg);

      return;
    }

    if (rc == '.') {
      if (DEBUG) {
        Serial.println("[. char]");
      }

      switch (commandState) {
        case READING_COMMAND:
          commandState = READING_R;
          break;
        case READING_R:
          commandState = READING_G;
          command.r = String(command.currentArg);
          break;
        case READING_G:
          commandState = READING_B;
          command.g = String(command.currentArg);
          break;
        default:
          break;
      }

      command.currentArg = String("");
      return;
    }

    if (commandState == READING_COMMAND) {
      if (rc >= 'a' && rc <= 'z') {
        command.command = String(command.command + rc);

        if (DEBUG) {
          Serial.print("[readingCommand] ");
          Serial.println(command.command);
        }
      }
      return;
    }

    if (readingCommandArg(commandState)) {
      if (rc >= '0' && rc <= '9') {
        command.currentArg = String(command.currentArg + rc);

        if (DEBUG) {
          Serial.print("[readingArg] ");
          Serial.println(command.currentArg);
        }
      }
      return;
    }
  }
}

void parseBluetooth() {
  if (commandState == NEW_DATA) {
    if (DEBUG) {
      Serial.print("Command : ");
      Serial.println(command.command);
    }

    if (command.command.compareTo("rgb") == 0) {
      moodLed.r = command.r.toInt();
      moodLed.g = command.g.toInt();
      moodLed.b = command.b.toInt();

      Serial.print("red : ");
      Serial.println(moodLed.r);

      Serial.print("green : ");
      Serial.println(moodLed.g);

      Serial.print("blue : ");
      Serial.println(moodLed.b);
    } else if ( command.command.compareTo("notif") == 0) {
      notifLed.r = command.r.toInt();
      notifLed.g = command.g.toInt();
      notifLed.b = command.b.toInt();

      notifLedDuration = 0;

      Serial.print("red : ");
      Serial.println(notifLed.r);

      Serial.print("green : ");
      Serial.println(notifLed.g);

      Serial.print("blue : ");
      Serial.println(notifLed.b);
    }

    commandState = READING_COMMAND;
    command = {};
  }
}

void updateLeds() {
  analogWrite(moodPins.r, moodLed.r * pulseMoodLed);
  analogWrite(moodPins.g, moodLed.g * pulseMoodLed);
  analogWrite(moodPins.b, moodLed.b * pulseMoodLed);

  if (notifLedDuration < MAX_NOTIF_LED_DURATION) {
    analogWrite(notifPins.r, notifLed.r);
    analogWrite(notifPins.g, notifLed.g);
    analogWrite(notifPins.b, notifLed.b);
  } else {
    analogWrite(notifPins.r, 0);
    analogWrite(notifPins.g, 0);
    analogWrite(notifPins.b, 0);
  }
}

void loop() {
  readSerial();
  readBluetooth();
  parseBluetooth();

  readPotentiometer();

  if (doNotDisturbButton.released()) {
    Serial.println("Button released");
    BTserial.println("button.released");
  }

  updateLeds();

  unsigned long timer = millis();
  unsigned long mod = timer % PULSE_LEN;
  pulseMoodLed = 0.5f * (1.0f + sin(2.0f * PI * (mod / (float)PULSE_LEN) - PI * 0.5f));
}
